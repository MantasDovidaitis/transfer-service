package services;

import static java.util.Optional.ofNullable;

import java.io.File;

import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {
    public static final String WEBAPP_DIR_LOCATION = "src/main/webapp/";
    private static Logger LOG = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) throws Exception {
        Tomcat tomcat = new Tomcat();
        String webPort = ofNullable(System.getenv("PORT")).orElse("8090");
        tomcat.setPort(Integer.parseInt(webPort));
        System.out.println(webPort);

        StandardContext ctx = (StandardContext) tomcat.addWebapp("/", new File(WEBAPP_DIR_LOCATION).getAbsolutePath());
        System.out.println("configuring app with basedir: " + new File("./" + WEBAPP_DIR_LOCATION).getAbsolutePath());

        File additionWebInfClasses = new File("target/classes");
        WebResourceRoot resources = new StandardRoot(ctx);
        resources.addPreResources(new DirResourceSet(resources, "/WEB-INF/classes",
            additionWebInfClasses.getAbsolutePath(), "/"));
        ctx.setResources(resources);

        tomcat.start();
        LOG.info("Tomcat started, waiting for request to http://localhost:{}/", webPort);

        tomcat.getServer().await();
    }
}