package services.transfer;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

public class NotEnoughFundsException extends ClientErrorException {

    public NotEnoughFundsException(String message) {
        super(message, Response.Status.BAD_REQUEST);
    }

}
