package services.transfer;

import static java.util.Objects.requireNonNull;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class AccountEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String ownerName;

    private String accountNumber;

    private BigDecimal balance;

    public AccountEntity() {
    }

    public AccountEntity(String ownerName, String accountNumber, BigDecimal balance) {
        this.ownerName = requireNonNull(ownerName, "Parameter ownerName is required");
        this.accountNumber = requireNonNull(accountNumber, "Parameter accountNumber is required");
        this.balance = requireNonNull(balance, "Parameter balance is required");
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public boolean withdrawal(BigDecimal deposit) {
        if (balance.compareTo(deposit) < 0) {
            throw new NotEnoughFundsException("No Money");
        }

        balance = balance.subtract(deposit);

        return true;
    }

    public boolean deposit(BigDecimal deposit) {
        balance = balance.add(deposit);

        return true;
    }

}
