package services.transfer;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import services.Injector;
import services.TransactionManager;

public class TransferApplicationService implements TransferEndpoints {

    private final AccountRepository accountRepository;

    private final AccountOutputGenerator accountOutputGenerator;

    private final TransactionManager transactionManager;

    public TransferApplicationService() {
        this.accountRepository = Injector.inject(AccountRepository.class);
        this.accountOutputGenerator = Injector.inject(AccountOutputGenerator.class);
        this.transactionManager = Injector.inject(TransactionManager.class);

    }

    public TransferApplicationService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
        this.accountOutputGenerator = Injector.inject(AccountOutputGenerator.class);
        this.transactionManager = Injector.inject(TransactionManager.class);
    }

    @Override
    @Transactional
    public MoneyTransferOutput transfer(MoneyTransferInput input) {
        EntityManager entityManager = transactionManager.beginTransaction();
        boolean isSuccess = true;
        try {
            isSuccess = isSuccess & accountRepository.modify(input.getFromAccount(), entity -> entity.withdrawal(input.getTransferAmount()),entityManager);
            isSuccess = isSuccess & accountRepository.modify(input.getToAccount(), entity -> entity.deposit(input.getTransferAmount()), entityManager);
            transactionManager.commitTransaction(entityManager);
        } finally {
            transactionManager.rollbackTransaction(entityManager);
        }

        return ImmutableMoneyTransferOutput.builder()
                   .success(isSuccess)
                   .build();
    }

    @Override
    @Transactional
    public List<AccountOutput> populateAndGet() {
        List<AccountEntity> accountOutputs;

        EntityManager entityManager = transactionManager.beginTransaction();
        try {
            if (accountRepository.findAll(entityManager).size() == 0) {
                accountRepository.save(new AccountEntity("Mantas", "LT12312321", BigDecimal.valueOf(100L)), entityManager);
                accountRepository.save(new AccountEntity("Petras", "LT3412312312", BigDecimal.valueOf(100L)), entityManager);
                accountRepository.save(new AccountEntity("Gediminas", "LT3523412312", BigDecimal.valueOf(100L)), entityManager);
                transactionManager.commitTransaction(entityManager);
            }

            accountOutputs = accountRepository.findAll(transactionManager.getEntityManager());
        } finally {
            transactionManager.rollbackTransaction(entityManager);
        }

        return accountOutputGenerator.generateAccountOutput(accountOutputs);
    }
}
