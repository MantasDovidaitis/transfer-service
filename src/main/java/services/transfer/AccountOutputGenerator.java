package services.transfer;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;

import java.util.List;

import services.transfer.TransferEndpoints.AccountOutput;

public class AccountOutputGenerator {

    public List<AccountOutput> generateAccountOutput(List<AccountEntity> accountEntities) {
        return newArrayList(accountEntities.stream()
                                .map(entity -> ImmutableAccountOutput.builder()
                                                   .accountNumber(entity.getAccountNumber())
                                                   .balance(entity.getBalance())
                                                   .ownerName(entity.getOwnerName())
                                                   .build())
                                .collect(toList()));
    }
}
