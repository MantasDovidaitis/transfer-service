package services.transfer;

import java.math.BigDecimal;
import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public interface TransferEndpoints {

    MoneyTransferOutput transfer(MoneyTransferInput input);

    List<AccountOutput> populateAndGet();

    @Value.Immutable
    @JsonSerialize(as = ImmutableMoneyTransferOutput.class)
    interface MoneyTransferOutput {

        boolean getSuccess();
    }

    @Value.Immutable
    @JsonDeserialize(as = ImmutableMoneyTransferInput.class)
    interface MoneyTransferInput {

        String getFromAccount();

        String getToAccount();

        BigDecimal getTransferAmount();
    }

    @Value.Immutable
    @JsonDeserialize(as = ImmutableAccountOutput.class)
    interface AccountOutput {
        String getOwnerName();

        String getAccountNumber();

        BigDecimal getBalance();
    }
}
