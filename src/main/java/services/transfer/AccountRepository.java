package services.transfer;

import java.util.List;
import java.util.function.Function;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

public class AccountRepository {

    public AccountRepository() {
    }

    public void save(AccountEntity object, EntityManager entityManager) {
        entityManager.persist(object);
    }

    public boolean modify(String accountNo, Function<AccountEntity, Boolean> modification, EntityManager entityManager) {
        AccountEntity entity = findByAccountNo(accountNo, entityManager);
        Boolean apply = modification.apply(entity);
        entityManager.merge(entity);

        return apply;
    }

    public List<AccountEntity> findAll(EntityManager entityManager) {
        Query query = entityManager.createQuery("SELECT a FROM AccountEntity a");
        return query.getResultList();
    }

    private AccountEntity findByAccountNo(String accountNo, EntityManager entityManager) {
        Query query = entityManager.createQuery("SELECT a FROM AccountEntity a WHERE accountNumber = :accountNo");
        query.setParameter("accountNo", accountNo);

        try {
            return (AccountEntity) query.getSingleResult();
        } catch (NoResultException e) {
            throw new ClientErrorException("Account Does not exist", Response.Status.NOT_FOUND);
        }
    }
}
