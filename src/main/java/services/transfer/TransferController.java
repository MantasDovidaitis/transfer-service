package services.transfer;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import services.Injector;

@Path("")
public class TransferController implements TransferEndpoints {

    private final TransferEndpoints transferApplicationService;

    public TransferController() {
        transferApplicationService = Injector.inject(TransferApplicationService.class);
    }

    public TransferController(TransferEndpoints transferApplicationService) {
        this.transferApplicationService = transferApplicationService;
    }

    @POST
    @Path("/transfer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public MoneyTransferOutput transfer(MoneyTransferInput input) {
        return transferApplicationService.transfer(input);
    }

    @PUT
    @Path("/populate")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public List<AccountOutput> populateAndGet() {
        return transferApplicationService.populateAndGet();
    }

}
