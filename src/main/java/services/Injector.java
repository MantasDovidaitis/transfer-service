package services;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.FutureTask;

public class Injector {
    private static ConcurrentHashMap<String, FutureTask<?>> container = new ConcurrentHashMap<>();

    public static <E> E inject(Class<E> classType) {
        try {
            FutureTask<E> future = (FutureTask<E>) container.get(classType.getName());
            if (future == null) {
                FutureTask<E> ft = new FutureTask<>(classType::newInstance);
                future = (FutureTask<E>) container.putIfAbsent(classType.getName(), ft);
                if (future == null) {
                    future = ft;
                    future.run();
                }
            }
            return future.get();
        } catch (Exception e) {
            throw new InjectorException("Error while trying to inject class", e);
        }
    }
}
