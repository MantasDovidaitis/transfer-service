package services;

import javax.ws.rs.InternalServerErrorException;

public class InjectorException extends InternalServerErrorException {

    public InjectorException(String message, Throwable cause) {
        super(message, cause);
    }

}
