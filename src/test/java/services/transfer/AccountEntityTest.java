package services.transfer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.math.BigDecimal;

import org.junit.Test;

public class AccountEntityTest {

    @Test
    public void shouldConstructAccountEntityWithGivenOwnerName() {
        // given
        String ownerName = "Mantas";

        // when
        AccountEntity accountEntity = new AccountEntity(ownerName, "LT123121", BigDecimal.TEN);

        // then
        assertThat(accountEntity.getOwnerName()).isEqualTo(ownerName);
    }

    @Test
    public void shouldThrowExceptionWhenOwnerNameIsNull() {
        assertThatThrownBy(() -> new AccountEntity(null, "LT123121", BigDecimal.TEN))
            .isInstanceOf(NullPointerException.class);
    }

    @Test
    public void shouldConstructAccountEntityWithGivenAccountNumber() {
        // given
        String accountNumber = "LT123121";

        // when
        AccountEntity accountEntity = new AccountEntity("Mantas", accountNumber, BigDecimal.TEN);

        // then
        assertThat(accountEntity.getAccountNumber()).isEqualTo(accountNumber);
    }

    @Test
    public void shouldThrowExceptionWhenAccountNumberIsNull() {
        assertThatThrownBy(() -> new AccountEntity("Mantas", null, BigDecimal.TEN))
            .isInstanceOf(NullPointerException.class);
    }

    @Test
    public void shouldConstructAccountEntityWithGivenBalance() {
        // given
        BigDecimal balance = BigDecimal.valueOf(12L);

        // when
        AccountEntity accountEntity = new AccountEntity("Mantas", "LT123121", balance);

        // then
        assertThat(accountEntity.getBalance()).isEqualTo(balance);
    }

    @Test
    public void shouldThrowExceptionWhenBalanceIsNull() {
        assertThatThrownBy(() -> new AccountEntity("Mantas", "LT123121", null))
            .isInstanceOf(NullPointerException.class);
    }

    @Test
    public void shouldWithdrawFromAccountAskedAmount() {
        // given
        AccountEntity accountEntity = new AccountEntity("Mantas", "LT123121", BigDecimal.valueOf(100L));

        // when
        accountEntity.withdrawal(BigDecimal.valueOf(15L));

        // then
        assertThat(accountEntity.getBalance()).isEqualTo(BigDecimal.valueOf(85L));
    }

    @Test
    public void shouldThrowExceptionWhenWithdrawnAmmountIsBiggerThanBalance() {
        // given
        AccountEntity accountEntity = new AccountEntity("Mantas", "LT123121", BigDecimal.valueOf(100L));

        // then
        assertThatThrownBy(() -> accountEntity.withdrawal(BigDecimal.valueOf(150L))).isInstanceOf(NotEnoughFundsException.class);
    }

    @Test
    public void shouldDepositToAccountAskedAmount() {
        // given
        AccountEntity accountEntity = new AccountEntity("Mantas", "LT123121", BigDecimal.valueOf(100L));

        // when
        accountEntity.deposit(BigDecimal.valueOf(15L));

        // then
        assertThat(accountEntity.getBalance()).isEqualTo(BigDecimal.valueOf(115L));
    }
}