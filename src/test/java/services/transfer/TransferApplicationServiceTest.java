package services.transfer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import services.transfer.TransferEndpoints.MoneyTransferOutput;

@RunWith(MockitoJUnitRunner.class)
public class TransferApplicationServiceTest {

    @Mock
    private AccountRepository accountRepository;

    private TransferApplicationService transferApplicationService;

    @Before
    public void setUp() {
        transferApplicationService = new TransferApplicationService(accountRepository);
    }

    @Test
    public void shouldReturnTrueWhenTransferingMoneyAndWithdrawalAndDepositAreSuccess() {
        // given
        String fromAccount = "LT12123123";
        String toAccount = "LT123123235345";

        given(accountRepository.modify(eq(fromAccount), any(), any()))
            .willReturn(true);
        given(accountRepository.modify(eq(toAccount), any(), any()))
            .willReturn(true);

        // when
        MoneyTransferOutput output =
            transferApplicationService.transfer(ImmutableMoneyTransferInput.builder()
                                                    .fromAccount(fromAccount)
                                                    .toAccount(toAccount)
                                                    .transferAmount(BigDecimal.valueOf(123L))
                                                    .build());

        // then
        assertThat(output.getSuccess()).isEqualTo(true);
    }

    @Test
    public void shouldReturnFalseWhenTransferingMoneyAndWithdrawalFails() {
        // given
        String fromAccount = "LT12123123";
        String toAccount = "LT123123235345";

        given(accountRepository.modify(eq(fromAccount), any(), any()))
            .willReturn(false);
        given(accountRepository.modify(eq(toAccount), any(), any()))
            .willReturn(true);

        // when
        MoneyTransferOutput output =
            transferApplicationService.transfer(ImmutableMoneyTransferInput.builder()
                                                    .fromAccount(fromAccount)
                                                    .toAccount(toAccount)
                                                    .transferAmount(BigDecimal.valueOf(123L))
                                                    .build());

        // then
        assertThat(output.getSuccess()).isEqualTo(false);
    }

    @Test
    public void shouldReturnFalseWhenTransferingMoneyAndDepositFails() {
        // given
        String fromAccount = "LT12123123";
        String toAccount = "LT123123235345";

        given(accountRepository.modify(eq(fromAccount), any(), any()))
            .willReturn(true);
        given(accountRepository.modify(eq(toAccount), any(), any()))
            .willReturn(false);

        // when
        MoneyTransferOutput output =
            transferApplicationService.transfer(ImmutableMoneyTransferInput.builder()
                                                    .fromAccount(fromAccount)
                                                    .toAccount(toAccount)
                                                    .transferAmount(BigDecimal.valueOf(123L))
                                                    .build());

        // then
        assertThat(output.getSuccess()).isEqualTo(false);
    }

}
