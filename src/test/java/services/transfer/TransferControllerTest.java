package services.transfer;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;

import java.math.BigDecimal;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class TransferControllerTest extends JerseyTest {

    @Mock
    private TransferApplicationService transferApplicationService;

    @Override
    protected Application configure() {
        MockitoAnnotations.initMocks(this);

        ResourceConfig config = new ResourceConfig();
        config.register(new TransferController(transferApplicationService));
        return config;
    }

    @Test
    public void shouldReturnSuccessForGivenRequest() {
        // given
        ImmutableMoneyTransferInput request = ImmutableMoneyTransferInput.builder()
                                                  .transferAmount(BigDecimal.TEN)
                                                  .fromAccount("LT1231231")
                                                  .toAccount("LT2342342")
                                                  .build();
        given(transferApplicationService.transfer(request))
            .willReturn(ImmutableMoneyTransferOutput.builder()
                            .success(true)
                            .build());

        // when
        Response response = target("transfer").request()
                                .post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE));

        // then
        assertThat(response.readEntity(String.class)).isEqualTo("{\"success\":true}");
    }

}
