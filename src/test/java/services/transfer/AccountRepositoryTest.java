package services.transfer;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;

import services.Injector;
import services.TransactionManager;

public class AccountRepositoryTest {

    private AccountRepository accountRepository;
    private TransactionManager transactionManager;

    @Before
    public void setUp() {
        accountRepository = Injector.inject(AccountRepository.class);
        transactionManager = new TransactionManager();
    }

    @Test
    public void shouldSaveAccountEntity() {
        // given
        AccountEntity accountEntity = new AccountEntity("Mantas", "LT123121", BigDecimal.valueOf(100));
        EntityManager entityManager = transactionManager.beginTransaction();

        // when
        accountRepository.save(accountEntity, entityManager);
        transactionManager.commitTransaction(entityManager);

        // then
        AccountEntity resultEntity = accountRepository.findAll(transactionManager.getEntityManager()).get(0);
        assertThat(resultEntity.getOwnerName()).isEqualTo(accountEntity.getOwnerName());
        assertThat(resultEntity.getAccountNumber()).isEqualTo(accountEntity.getAccountNumber());
    }

    @Test
    public void shouldFindAllAccountEntities() {
        // given
        EntityManager entityManager = transactionManager.beginTransaction();
        accountRepository.save(new AccountEntity("Mantas", "LT1231211", BigDecimal.valueOf(100)), entityManager);
        accountRepository.save(new AccountEntity("Mantas", "LT1231212", BigDecimal.valueOf(100)), entityManager);
        accountRepository.save(new AccountEntity("Mantas", "LT1231213", BigDecimal.valueOf(100)), entityManager);
        transactionManager.commitTransaction(entityManager);

        // when
        List<AccountEntity> accountEntities = accountRepository.findAll(transactionManager.getEntityManager());

        // then
        assertThat(accountEntities).hasSize(3);
    }

    @Test
    public void shouldModifyAccountEntitiesBalance() {
        // given
        String accountNo = "LT1231211";
        EntityManager entityManager = transactionManager.beginTransaction();
        accountRepository.save(new AccountEntity("Mantas", accountNo, BigDecimal.valueOf(100)), entityManager);


        // when
        accountRepository.modify(accountNo, entity -> entity.deposit(BigDecimal.valueOf(10L)), entityManager);
        transactionManager.commitTransaction(entityManager);

        // then
        assertThat(accountRepository.findAll(transactionManager.getEntityManager()).get(0).getBalance()).isEqualTo(BigDecimal.valueOf(110).setScale(2));
    }
}
